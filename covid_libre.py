#bibliothèques
from tkinter import *
from random import randrange
import numpy as np 
#On cree une fenetre et un canevas:
tk = Tk()
#détermine les dimensions du canevas, les variables les rendent facilement modifiable
width = 800
height = 700
canvas = Canvas(tk,width = width, height = height , bd=0, bg="white")
canvas.pack(padx=10,pady=10)

def choc(b1, b2):
    #b["x","y","vx","vy"]
    x1, x2 , y1, y2 = b1['x'], b2['x'], b1['y'], b2['y']
    vx1, vx2, vy1, vy2 = b1['vx'], b2['vx'], b1['vy'], b2['vy']
    dx = x2 - x1 + 0.0000001
    dy = y2 - y1 + 0.0000001
    rayon = 10
    dist = np.sqrt(dx**2 + dy**2)
    if dist <= 2.1*rayon:
        nx, ny = dx/dist, dy/dist # vecteur normal unitaire 
        tx , ty = -ny, nx # vecteur tangente unitaire
        vn1 = (vx1*nx + vy1*ny) # projection 1 normale
        vt1 = (vx1*tx + vy1*ty) # projection 1 tangentielle 
        vn2 = (vx2*nx + vy2*ny) # projection 2 normale
        vt2 = (vx2*tx + vy2*ty) # projection 2 tangentielle
        vnP1, vnP2 = vn2, vn1 # les vecteurs normaux sont échangés comme un choc 1D
        vtP1, vtP2 = vt1, vt2 # vecteurs tan inchangés
        vx1 = vnP1*nx + vt1*tx # v = vn + vt
        vy1 = vnP1*ny + vt1*ty
        vx2 = vnP2*nx + vt2*tx
        vy2 = vnP2*ny + vt2*ty
    return(vx1,vy1,vx2,vy2)

def deplacement():
    global ls_balle
    for balle in ls_balle:
        if canvas.coords(balle[0])[0]<=0 or canvas.coords(balle[0])[2]>=width:
            balle[1]=-balle[1]
        elif canvas.coords(balle[0])[1]<=0 or canvas.coords(balle[0])[-1]>=height:
            balle[2]=-balle[2]
        #permet de savoir quand les billes se touchent
        chevauchement=canvas.find_overlapping(canvas.coords(balle[0])[0],canvas.coords(balle[0])[1],canvas.coords(balle[0])[2],canvas.coords(balle[0])[-1])
        if len(chevauchement)!=1 :
            for ball in ls_balle:
                if chevauchement[1]==ball[0]:
                    b1={"x":(canvas.coords(balle[0])[0]+canvas.coords(balle[0])[2])/2,"y":(canvas.coords(balle[0])[1]+canvas.coords(balle[0])[-1])/2,"vx":balle[1],"vy":balle[2]}
                    b2={"x":(canvas.coords(ball[0])[0]+canvas.coords(ball[0])[2])/2,"y":(canvas.coords(ball[0])[1]+canvas.coords(ball[0])[-1])/2,"vx":ball[1],"vy":ball[2]}
                    balle[1],balle[2],ball[1],ball[2]=choc(b1,b2) 
                    if canvas.itemcget(balle[0],"fill")=="brown" or canvas.itemcget(ball[0],"fill")=="brown":
                        canvas.itemconfigure(balle[0],fill="brown")
                        canvas.itemconfigure(ball[0],fill="brown")

        canvas.move(balle[0],balle[1],balle[2])
        #On repete cette fonction
    tk.after(20,deplacement)

#Creation  d'un bouton "Quitter":
Bouton_Quitter=Button(tk, text ='Quitter', command = tk.destroy)
#On ajoute l'affichage du bouton dans la fenêtre tk:
Bouton_Quitter.pack()

#On cree les balles avec position et vitesse aléatoire et couleur:
ls_balle=[]
for i in range(40):#détermine le nombre de balle
    #[balle,dx,dy]
    x1,y1=randrange(10,width),randrange(10,height)#position
    balle = canvas.create_oval(x1,y1,x1+10,y1+10,fill='white')#couleur
    dx,dy=randrange(-10,10),randrange(-5,5)#vitesse
    ls_balle+=[[balle,dx,dy]]
x1,y1=randrange(10,width),randrange(10,height)#position
balle = canvas.create_oval(x1,y1,x1+10,y1+10,fill='brown')#couleur
dx,dy=randrange(-10,10),randrange(-10,10)#vitesse
ls_balle+=[[balle,dx,dy]]
deplacement()
#On lance la boucle principale:
tk.mainloop()
